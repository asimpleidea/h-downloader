# Image URL to use all building/pushing image targets
IMG ?= {CONTAINER_IMAGE}
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"

ifeq ($(IMG),{CONTAINER_IMAGE})
IMG := asimpleidea/h-downloader:latest
endif

build:
	go build main.go -o h

# Build the docker image
docker-build: test
	docker build . -t ${IMG}

# Push the docker image
docker-push:
	docker push ${IMG}

gomod:
	go mod download && go mod tidy && go mod verify
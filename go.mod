module gitlab.com/asimpleidea/h-downloader

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/gen2brain/beeep v0.0.0-20210529141713-5586760f0cc1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rs/zerolog v1.26.1
	github.com/spf13/cobra v1.3.0
	golang.design/x/clipboard v0.5.3
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/go-toast/toast v0.0.0-20190211030409-01e6764cf0a4 // indirect
	github.com/godbus/dbus/v5 v5.0.4 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20180825215210-0210a2f0f73c // indirect
	github.com/gopherjs/gopherwasm v1.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	golang.org/x/exp v0.0.0-20200224162631-6cc2880d07d6 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/mobile v0.0.0-20210716004757-34ab1303b554 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
	golang.org/x/sys v0.0.0-20211205182925-97ca703d548d // indirect
)

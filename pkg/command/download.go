// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package command

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/asimpleidea/h-downloader/pkg/downloaders"
)

const (
	hentaiFoundry string = "www.hentai-foundry.com"
	gelbooru      string = "gelbooru.com"
	exhentai      string = "exhentai.org"
)

func getDownloadCommand() *cobra.Command {
	opts := &downloaders.DownloadOptions{}
	wopts := &downloaders.WorkerOptions{}

	cmd := &cobra.Command{
		Use:     "download [website] [OPTIONS]",
		Short:   "Download something locally",
		Long:    "Download a media locally, by parsing the website's data.",
		Aliases: []string{"dw"},
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				return fmt.Errorf("no websites provided")
			}

			if wopts.Max < 1 {
				return fmt.Errorf("invalid workers number")
			}

			if wopts.Pause < 0 {
				return fmt.Errorf("invalid pause")
			}

			if wopts.Batch < 1 {
				return fmt.Errorf("invalid batch number")
			}

			return runDownloadCommand(args[0], wopts, opts)
		},
		Example: "download http://example.com/image.jpg --artist this --ss --rank 3 --tags this,that --output pos",
	}

	// Flags
	cmd.Flags().StringVarP(&opts.Artist, "artist", "a", "", "artist override")
	cmd.Flags().StringVarP(&opts.Destination, "dest", "d", ".", "the path where to download this")
	cmd.Flags().DurationVarP(&wopts.Pause, "pause", "p", 30*time.Second, "time to wait between consecutive groups of downloads")
	cmd.Flags().DurationVarP(&wopts.Timeout, "timeout", "t", 2*time.Minute, "time for each download to complete")
	cmd.Flags().IntVarP(&wopts.Max, "workers", "w", 1, "number of workers to do in parallel")
	cmd.Flags().IntVarP(&wopts.Batch, "batch", "b", 1, "number of elements to download before taking a pause")

	return cmd
}

func runDownloadCommand(u string, wopts *downloaders.WorkerOptions, opts *downloaders.DownloadOptions) error {
	log := zerolog.New(zerolog.NewConsoleWriter())
	downchan := make(chan downloaders.Downloader, 100)
	resChan := make(chan *downloaders.DownloadResult, 100)
	urlPage, err := url.Parse(u)
	if err != nil {
		return fmt.Errorf("error while parsing url: %w", err)
	}
	downloaders.DownloadsCache = cache.New(30*time.Minute, 10*time.Minute)

	if _, err := downloaders.NewCookiesManager(); err != nil {
		return fmt.Errorf("error while getting cookies manager")
	}

	supportedHosts := map[string]bool{
		hentaiFoundry: true,
		gelbooru:      true,
		exhentai:      true,
	}
	wg := sync.WaitGroup{}

	if _, exists := supportedHosts[urlPage.Host]; !exists {
		return fmt.Errorf("unsupported host")
	}

	ctx, canc := context.WithCancel(context.Background())
	wopts.ResChan = resChan

	for i := 0; i < wopts.Max; i++ {
		wg.Add(1)
		go func(wid int) {
			defer wg.Done()
			if err := downloaders.Work(ctx, wid, wopts, downchan); err != nil {
				log.Info().Int("worker#", wid).Msg("error while downloading ")
			}
		}(i)
	}

	var it downloaders.LinksIterator
	switch urlPage.Host {
	case hentaiFoundry:
		it, err = downloaders.NewHentaiFoundryDownloader(u, opts)
	case gelbooru:
		it, err = downloaders.NewGelbooruDownloader(u, opts)
	case exhentai:
		it, err = downloaders.NewExhentaiDownloader(u, opts)
	default:
		canc()
		return fmt.Errorf("uknown hoster")
	}

	if err != nil {
		canc()
		return err
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	filesToDownload := 0
	filesTried := 0
	wg.Add(1)
	go func() {
		defer wg.Done()

		for {
			dwCtx, dwCanc := context.WithTimeout(ctx, time.Minute)
			dw, err := it.Next(dwCtx)
			if err != nil {
				if !errors.Is(err, downloaders.IteratorDone) {
					log.Err(err).Msg("error while getting next link")
				} else {
					log.Info().Int("#", filesToDownload).Msg("all links retrieved")
				}

				if filesToDownload == 0 {
					close(stop)
				}

				dwCanc()
				break
			}

			dwCanc()
			filesToDownload++
			downchan <- dw
		}
	}()

	successfulDownloads := 0
	for {
		select {
		case <-stop:
			log.Info().Msg("cancel requested")
			// Graceful break
			filesTried = filesToDownload + 1
		case dwRes := <-resChan:
			if dwRes.Error != nil {
				log.Err(dwRes.Error).
					Int("worker", dwRes.FromWorker).
					Msg("could not download media")
			} else {
				successfulDownloads++
				log.Info().
					Int("worker", dwRes.FromWorker).
					Str("url", dwRes.URL).
					Str("filepath", dwRes.FilePath).
					Msg("finished")
			}
			// TODO: beeep.Beep()
			filesTried++
		}

		if filesTried >= filesToDownload {
			break
		}
	}

	if filesToDownload > 0 && filesTried <= filesToDownload {
		log.Info().
			Int("successful-downloads", successfulDownloads).
			Int("files-to-download", filesToDownload).
			Msg("finished")
	}

	log.Info().Msg("shutting down...")
	close(downchan)
	canc()

	log.Info().Msg("waiting for all workers to terminate...")
	wg.Wait()

	log.Info().Msg("goodbye!")

	return nil
}

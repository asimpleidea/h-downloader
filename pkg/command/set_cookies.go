// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package command

import (
	"fmt"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/asimpleidea/h-downloader/pkg/downloaders"
)

func getSetCookieCommand() *cobra.Command {
	var (
		domain string
		value  string
	)
	cmd := &cobra.Command{
		Use:   "cookie [WEBSITE] [OPTIONS]",
		Short: "Set a cookie",
		Long:  "Some cookie for a certain website.",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("no website and cookies set")
			}

			domain = args[0]
			value = args[1]
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return runSetCookie(domain, value)
		},
		Example: "set cookie gelbooru --name PHPSSESSID --content abc123",
	}

	// Flags

	return cmd
}

func runSetCookie(domain, value string) error {
	log := zerolog.New(zerolog.NewConsoleWriter())

	log.Info().Str("domain", domain).
		Str("value", value).
		Msg("trying to create cookie")

	mgr, err := downloaders.NewCookiesManager()
	if err != nil {
		return err
	}

	if err := mgr.Write(domain, value); err != nil {
		return err
	}

	log.Info().Msg("successfully wrote cookie")
	return nil
}

// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package command

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"path"
	"sync"
	"time"

	"github.com/gen2brain/beeep"
	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/asimpleidea/h-downloader/pkg/downloaders"
	"golang.design/x/clipboard"
)

func getWatchClipboardCommand() *cobra.Command {
	wopts := &downloaders.WorkerOptions{}
	dest := ""

	cmd := &cobra.Command{
		Use:   "watch-clipboard [OPTIONS]",
		Short: "Watch clipboard",
		RunE: func(cmd *cobra.Command, args []string) error {
			return runWatchClipboard(wopts, dest)
		},
		Example: "set cookie gelbooru --name PHPSSESSID --content abc123",
	}

	// Flags
	cmd.Flags().DurationVarP(&wopts.Pause, "pause", "p", 30*time.Second, "time to wait between consecutive groups of downloads")
	cmd.Flags().DurationVarP(&wopts.Timeout, "timeout", "t", 2*time.Minute, "time for each download to complete")
	cmd.Flags().IntVarP(&wopts.Max, "workers", "w", 1, "number of workers to do in parallel")
	cmd.Flags().IntVarP(&wopts.Batch, "batch", "b", 1, "number of elements to download before taking a pause")
	cmd.Flags().StringVarP(&dest, "dest", "d", ".", "where to download stuff")

	return cmd
}

func runWatchClipboard(wopts *downloaders.WorkerOptions, dest string) error {
	ex, err := os.Executable()
	if err != nil {
		return fmt.Errorf("error while getting executable directory: %w", err)
	}
	assetsPath := path.Join(path.Dir(ex), "assets")
	downloaders.DownloadsCache = cache.New(30*time.Minute, 10*time.Minute)

	if _, err := downloaders.NewCookiesManager(); err != nil {
		return fmt.Errorf("error while getting cookies manager")
	}

	log := zerolog.New(zerolog.NewConsoleWriter())
	log.Info().Msg("starting...")
	supportedHosts := map[string]bool{
		hentaiFoundry: true,
		gelbooru:      true,
		exhentai:      true,
	}
	ctx, canc := context.WithCancel(context.TODO())
	wg := sync.WaitGroup{}
	linkChan := make(chan *url.URL, 100)
	downChan := make(chan downloaders.Downloader, 100)
	resChan := make(chan *downloaders.DownloadResult, 100)

	wopts.ResChan = resChan

	for i := 0; i < wopts.Max; i++ {
		wg.Add(1)
		go func(wid int) {
			defer wg.Done()

			if err := downloaders.Work(ctx, wid, wopts, downChan); err != nil {
				log.Info().Int("worker#", wid).Msg("error while downloading")
			}

			log.Info().Int("worker#", wid).Msg("exiting...")
		}(i)
	}

	wg.Add(1)
	go func() {
		defer wg.Done()

		ch := clipboard.Watch(ctx, clipboard.FmtText)
		for data := range ch {
			u, err := url.Parse(string(data))
			if err != nil {
				continue
			}

			if _, exists := supportedHosts[u.Host]; exists {
				linkChan <- u
			}
		}

		log.Info().Str("worker", "clipboard-watcher").Msg("exiting...")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()

		for link := range linkChan {
			log.Info().Str("link", link.String()).Msg("caught link")
			if err := beeep.Notify("Caught link", fmt.Sprintf("link is %s", link.String()), path.Join(assetsPath, "link.png")); err != nil {
				log.Err(err).Msg("could not notify")
			}
			dopts := &downloaders.DownloadOptions{Destination: dest}

			var (
				it  downloaders.LinksIterator
				err error
			)
			switch link.Host {
			case hentaiFoundry:
				it, err = downloaders.NewHentaiFoundryDownloader(link.String(), dopts)
			case gelbooru:
				it, err = downloaders.NewGelbooruDownloader(link.String(), dopts)
			case exhentai:
				it, err = downloaders.NewExhentaiDownloader(link.String(), dopts)
			default:
				err = fmt.Errorf("unknown hoster")
			}
			if err != nil {
				log.Err(err).Msg("error while getting link, skipping...")
				continue
			}

			filesToDownload := 0
			for {
				dwCtx, dwCanc := context.WithTimeout(ctx, time.Minute)
				dw, err := it.Next(dwCtx)
				if err != nil {
					if !errors.Is(err, downloaders.IteratorDone) {
						log.Err(err).Msg("error while getting next link")
					} else {
						log.Info().Int("#", filesToDownload).Msg("all links retrieved")
					}

					dwCanc()
					break
				}

				dwCanc()
				filesToDownload++
				downChan <- dw
			}

			successfulDownloads := 0
			filesTried := 0
			for {
				select {
				case <-ctx.Done():
					return
				case dwRes := <-resChan:
					if dwRes.Error != nil {
						log.Err(dwRes.Error).
							Int("worker", dwRes.FromWorker).
							Msg("could not download media")
					} else {
						successfulDownloads++
					}
					// TODO: beeep.Beep()
					filesTried++
				}

				if filesTried >= filesToDownload {
					log.Info().Str("link", link.String()).Msg("all download finished")
					if err := beeep.Notify("Finished", fmt.Sprintf("from link %s", link.String()), path.Join(assetsPath, "done.png")); err != nil {
						log.Err(err).Msg("could not notify")
					}
					break
				}
			}
		}

		log.Info().Str("worker", "link-parser").Msg("exiting...")
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop

	log.Info().Msg("shutting down...")
	canc()
	close(linkChan)
	close(downChan)
	close(resChan)

	log.Info().Msg("waiting for all workers to terminate...")
	wg.Wait()

	log.Info().Msg("goodbye!")

	return nil
}

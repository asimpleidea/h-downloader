// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gopkg.in/yaml.v3"
)

const (
	cookiesDirName   string = "cookies"
	cookiesFileName  string = "cookies.yaml"
	cookieHeaderName string = "Cookie"
)

var cookiesManager *CookiesManager

type CookiesManager struct {
	cookiesDir string
	cookies    map[string]string
}

func NewCookiesManager() (*CookiesManager, error) {
	if cookiesManager != nil {
		return cookiesManager, nil
	}

	exeDir, err := os.Executable()
	if err != nil {
		return nil, err
	}

	cookiesDirPath := path.Join(path.Dir(exeDir), cookiesDirName)
	if _, err := createFolderIfNotExists(cookiesDirPath); err != nil {
		return nil, fmt.Errorf("error while creating directory: %w", err)
	}

	cookiesFile := path.Join(cookiesDirPath, cookiesFileName)
	cookiesList, err := func() (map[string]string, error) {
		file, err := os.Open(cookiesFile)
		if err != nil {
			if os.IsNotExist(err) {
				return map[string]string{}, nil
			}

			return nil, fmt.Errorf("error while loading cookie file: %w", err)
		}

		defer file.Close()
		vals := map[string]string{}
		return vals, yaml.NewDecoder(file).Decode(&vals)
	}()
	if err != nil {
		return nil, err
	}

	mgr := &CookiesManager{
		cookiesDir: cookiesDirPath,
		cookies:    cookiesList,
	}
	cookiesManager = mgr
	return mgr, nil
}

func (c *CookiesManager) Write(website, value string) error {
	if website == "" {
		return fmt.Errorf("no website set")
	}

	if value == "" {
		delete(c.cookies, website)
	} else {
		c.cookies[website] = value
	}

	data, err := yaml.Marshal(c.cookies)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path.Join(c.cookiesDir, cookiesFileName), data, 0775)
}

func (c *CookiesManager) Get(website string) string {
	return c.cookies[website]
}

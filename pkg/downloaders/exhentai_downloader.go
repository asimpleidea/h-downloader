// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"path"

	"github.com/PuerkitoBio/goquery"
	"github.com/patrickmn/go-cache"
)

type exhentaiPictureDownloader struct {
	url            string
	defaultOptions *DownloadOptions
}

func (e *exhentaiPictureDownloader) Download(ctx context.Context, opts *DownloadOptions) (*DownloadResult, error) {
	o := e.defaultOptions.Clone(opts)
	res := &DownloadResult{
		URL: e.url,
	}

	client := &http.Client{}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, e.url, nil)
	if err != nil {
		return res, fmt.Errorf("error while creating request: %w", err)
	}

	{
		u, _ := url.Parse(e.url)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		return res, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return res, fmt.Errorf("status code is not 200 but %d", resp.StatusCode)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return res, err
	}

	headTitle := doc.Find("head > title").Text()
	imgLink := ""
	{
		img := doc.Find("img#img")
		if img.Length() == 0 {
			return res, fmt.Errorf("no image found")
		}

		imgLink = img.AttrOr("src", "")
	}

	if imgLink == "" {
		return res, fmt.Errorf("image link not found")
	}

	if o.Artist == "" {
		if _artist, exists := DownloadsCache.Get(headTitle); exists {
			o.Artist = *(_artist.(*string))
		}

		o.Artist = getArtistNameFromExhentaiPictureURL(ctx, client, doc)
		DownloadsCache.Set(headTitle, &o.Artist, cache.DefaultExpiration)
	}

	folderName := func() string {
		title := doc.Find("head > title").Text()

		if len(title) > 200 {
			title = title[:200]
		}
		return title
	}()

	dest := path.Join(o.Destination, sanitizeNameForPath(o.Artist))
	res.FilePath = path.Join(dest, o.FileName)

	if _, err := createFolderIfNotExists(dest); err != nil {
		return res, fmt.Errorf("cannot create directory: %w", err)
	}

	dest = path.Join(dest, sanitizeNameForPath(folderName))
	if _, err := createFolderIfNotExists(dest); err != nil {
		return res, fmt.Errorf("cannot create directory: %w", err)
	}

	o.Destination = path.Join(dest, path.Base(imgLink))
	res.FilePath = o.Destination
	return res, downloadImage(client, imgLink, o)
}

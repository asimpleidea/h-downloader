// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

type exhentaiLinksIterator struct {
	defaultOpts *DownloadOptions
	links       []string
	nextPageURL string
	index       int
}

func NewExhentaiDownloader(webpage string, opts *DownloadOptions) (*exhentaiLinksIterator, error) {
	uri, err := url.Parse(webpage)
	if err != nil {
		return nil, fmt.Errorf("error while parsing url: %w", err)
	}

	downloadMode, err := parseExhentaiUrl(uri)
	if err != nil {
		return nil, err
	}

	if opts == nil {
		opts = &DownloadOptions{}
	}

	iterator := &exhentaiLinksIterator{
		defaultOpts: opts,
	}

	if downloadMode == singularMode {
		iterator.links = []string{uri.String()}
	} else {
		iterator.nextPageURL = uri.String()
	}

	return iterator, nil
}

func (h *exhentaiLinksIterator) Next(ctx context.Context) (Downloader, error) {
	for h.index < len(h.links) {
		dw := &exhentaiPictureDownloader{
			url:            h.links[h.index],
			defaultOptions: h.defaultOpts.Clone(nil),
		}

		h.index++
		return dw, nil
	}

	if h.nextPageURL != "" {
		nextPageURL, links, err := h.getPicturesFromExhentaiGallery(ctx, h.nextPageURL)
		if err != nil {
			h.nextPageURL = ""
			return nil, err
		}

		h.nextPageURL = nextPageURL
		h.links = append(h.links, links...)
		return h.Next(ctx)
	}

	return nil, IteratorDone
}

func (h *exhentaiLinksIterator) getPicturesFromExhentaiGallery(ctx context.Context, webpage string) (string, []string, error) {
	// Prepare the request
	client := &http.Client{}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, webpage, nil)
	if err != nil {
		return "", nil, fmt.Errorf("error while preparing request: %w", err)
	}

	{
		u, _ := url.Parse(webpage)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return "", nil, fmt.Errorf("error while getting webpage: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return "", nil, fmt.Errorf("status code is not 200 but %d", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", nil, err
	}

	if h.defaultOpts.Artist == "" {
		h.defaultOpts.Artist = getArtistFromExhentaiGalleryHTML(body)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
	if err != nil {
		return "", nil, err
	}

	// Get next page URL
	nextPageURL := func() string {
		currPageURL := doc.Find("td.ptds")
		if currPageURL.Length() == 0 {
			return ""
		}

		nextTd := currPageURL.First().Next()
		if nextTd.Length() == 0 {
			return ""
		}

		nextPage := nextTd.ChildrenFiltered("a")
		if nextPage.Length() == 0 {
			return ""
		}

		link := nextPage.AttrOr("href", "")
		if link == "" {
			return ""
		}

		return link
	}()

	// Get previews
	links := []string{}
	{
		thumbs := doc.Find("div#gdt > div.gdtl")
		if thumbs.Length() == 0 {
			return "", nil, fmt.Errorf("no links found on page")
		}

		for i := 0; i < thumbs.Length(); i++ {
			if thumb := goquery.NewDocumentFromNode(thumbs.Get(i)).ChildrenFiltered("a").First(); thumb.Length() > 0 {
				if link, exists := thumb.Attr("href"); exists && link != "" {
					links = append(links, link)
				}
			}
		}
	}

	return nextPageURL, links, nil
}

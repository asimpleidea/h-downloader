package downloaders

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func getArtistNameFromExhentaiPictureURL(ctx context.Context, client *http.Client, doc *goquery.Document) (artist string) {
	artist = doc.Find("head > title").Text()
	if artist == "" {
		artist = "unknown"
	}

	i5 := doc.Find("div#i5")
	if i5.Length() == 0 {
		return
	}

	galleryLink := i5.Find("a")
	if galleryLink.Length() == 0 {
		return
	}

	link := galleryLink.First().AttrOr("href", "")
	if link == "" {
		return
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, link, nil)
	if err != nil {
		return
	}

	{
		u, _ := url.Parse(link)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	artist = getArtistFromExhentaiGalleryHTML(body)
	return
}

func parseExhentaiUrl(uri *url.URL) (downloadMode, error) {
	switch {
	case strings.HasPrefix(uri.Path, "/g/"):
		return fullArtistMode, nil
	case strings.HasPrefix(uri.Path, "/s/"):
		return singularMode, nil
	default:
		return "", fmt.Errorf("unrecognized mode")
	}
}

func getArtistFromExhentaiGalleryHTML(body []byte) (artist string) {
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(body))
	if err != nil {
		return
	}

	tagsList := doc.Find("div#taglist")
	if tagsList.Length() == 0 {
		return
	}

	trs := tagsList.Find("tr")

	for _, tr := range trs.Nodes {
		for _, td := range goquery.NewDocumentFromNode(tr).ChildrenFiltered("td").Nodes {
			tdNode := goquery.NewDocumentFromNode(td)
			if !strings.HasPrefix(tdNode.Text(), "artist") {
				break
			}

			if artists := tdNode.Next().Find("a"); artists.Length() > 0 {
				return artists.First().Text()
			}
		}
	}

	return
}

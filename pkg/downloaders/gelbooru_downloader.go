// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type gelbooruPictureDownloader struct {
	url            string
	defaultOptions *DownloadOptions
}

func (g *gelbooruPictureDownloader) Download(ctx context.Context, opts *DownloadOptions) (*DownloadResult, error) {
	o := g.defaultOptions.Clone(opts)
	res := &DownloadResult{
		URL: g.url,
	}

	client := &http.Client{}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, g.url, nil)
	if err != nil {
		return res, fmt.Errorf("error while creating request: %w", err)
	}

	{
		u, _ := url.Parse(g.url)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	resp, err := client.Get(g.url)
	if err != nil {
		return res, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return res, fmt.Errorf("status code is not 200 but %d", resp.StatusCode)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return res, err
	}

	if o.Artist == "" {
		artistTag := doc.Find("li.tag-type-artist > a")
		if artistTag.Length() > 0 {
			o.Artist = strings.Trim(artistTag.First().Text(), " ")
		} else {
			o.Artist = "unknown"
		}
	}
	o.Artist = strings.Replace(o.Artist, "_", " ", -1)

	imgLink := ""
	{
		liElements := doc.Find("ul.tag-list > li")
		if liElements.Length() == 0 {
			return res, fmt.Errorf("no tag lists found")
		}

		for i := liElements.Length() - 1; i > 0; i-- {
			a := goquery.NewDocumentFromNode(liElements.Get(i)).ChildrenFiltered("a")
			if a.Length() == 0 {
				continue
			}

			if strings.ToLower(a.Text()) == "original image" {
				if link, exists := a.Attr("href"); exists && link != "" {
					imgLink = link
				}
			}
		}
	}

	if imgLink == "" {
		return res, fmt.Errorf("image link not found")
	}

	if o.FileName == "" {
		p, _ := url.Parse(imgLink)
		o.FileName = path.Base(p.Path)
	}
	dest := path.Join(o.Destination, sanitizeNameForPath(o.Artist))
	res.FilePath = path.Join(dest, o.FileName)

	if _, err := createFolderIfNotExists(dest); err != nil {
		return res, fmt.Errorf("cannot create directory: %w", err)
	}

	o.Destination = dest
	return res, downloadImage(client, imgLink, o)
}

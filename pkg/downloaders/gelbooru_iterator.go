// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type gelbooruLinksIterator struct {
	defaultOpts *DownloadOptions
	links       []string
	nextPageURL string
	index       int
}

func NewGelbooruDownloader(webpage string, opts *DownloadOptions) (*gelbooruLinksIterator, error) {
	uri, err := url.Parse(webpage)
	if err != nil {
		return nil, fmt.Errorf("error while parsing url: %w", err)
	}

	downloadMode, err := parseGelbooruURL(uri)
	if err != nil {
		return nil, err
	}

	if opts == nil {
		opts = &DownloadOptions{}
	}

	iterator := &gelbooruLinksIterator{
		defaultOpts: opts,
	}

	if downloadMode == singularMode {
		iterator.links = []string{uri.String()}
	} else {
		iterator.nextPageURL = uri.String()
		iterator.defaultOpts.Artist = getArtistNameFromGelbooruURL(uri)
	}

	return iterator, nil
}

func (g *gelbooruLinksIterator) Next(ctx context.Context) (Downloader, error) {
	for g.index < len(g.links) {
		dw := &gelbooruPictureDownloader{
			url:            g.links[g.index],
			defaultOptions: g.defaultOpts.Clone(nil),
		}

		g.index++
		return dw, nil
	}

	if g.nextPageURL != "" {
		nextPageURL, links, err := g.getPicturesFromGelbooruTag(ctx, g.nextPageURL)
		if err != nil {
			g.nextPageURL = ""
			return nil, err
		}

		g.nextPageURL = nextPageURL
		g.links = append(g.links, links...)
		return g.Next(ctx)
	}

	return nil, IteratorDone
}

func (g *gelbooruLinksIterator) getPicturesFromGelbooruTag(ctx context.Context, webpage string) (string, []string, error) {
	// Prepare the request
	client := &http.Client{}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, webpage, nil)
	if err != nil {
		return "", nil, fmt.Errorf("error while preparing request: %w", err)
	}

	{
		u, _ := url.Parse(webpage)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return "", nil, fmt.Errorf("error while getting webpage: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return "", nil, fmt.Errorf("status code is not 200 but %d", res.StatusCode)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return "", nil, err
	}

	// Get next page URL
	nextPageURL := func() string {
		paginator := doc.Find("div#paginator")
		if paginator.Length() == 0 {
			return ""
		}

		currPage := paginator.Find("b")
		if currPage.Length() == 0 {
			return ""
		}

		nextPage := currPage.First().NextFiltered("a")
		if nextPage.Length() == 0 {
			return ""
		}

		href, exists := nextPage.Attr("href")
		if !exists || href == "" {
			return ""
		}

		href = strings.Trim(href, "?")

		return (&url.URL{
			Scheme:   "https",
			Host:     "gelbooru.com",
			Path:     "index.php",
			RawQuery: href,
		}).String()
	}()

	// Get previews
	links := []string{}
	{
		previews := doc.Find("article.thumbnail-preview")
		if previews.Length() == 0 {
			return "", nil, fmt.Errorf("no links found on page")
		}

		for i := 0; i < previews.Length(); i++ {
			if thumb := goquery.NewDocumentFromNode(previews.Get(i)).ChildrenFiltered("a").First(); thumb.Length() > 0 {
				if link, exists := thumb.Attr("href"); exists && link != "" {
					links = append(links, link)
				}
			}
		}
	}

	return nextPageURL, links, nil
}

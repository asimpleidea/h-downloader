// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"fmt"
	"net/url"
	"strings"
)

func parseGelbooruURL(uri *url.URL) (downloadMode, error) {
	page := uri.Query().Get("page")
	if page != "post" {
		return "", fmt.Errorf("unrecognized gelbooru URL")
	}

	switch s := uri.Query().Get("s"); s {
	case "view":
		return singularMode, nil
	case "list":
		return fullArtistMode, nil
	default:
		return "", fmt.Errorf("unrecognized gelbooru URL")
	}
}

func getArtistNameFromGelbooruURL(u *url.URL) string {
	tag := u.Query().Get("tags")

	return strings.Replace(tag, "_", " ", -1)
}

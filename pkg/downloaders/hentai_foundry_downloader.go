// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"path"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type hentaiFoundryPictureDownloader struct {
	url            string
	defaultOptions *DownloadOptions
}

func (h *hentaiFoundryPictureDownloader) Download(ctx context.Context, opts *DownloadOptions) (*DownloadResult, error) {
	o := h.defaultOptions.Clone(opts)
	res := &DownloadResult{
		URL: h.url,
	}
	h.url = insertEnterAgreeOnURL(h.url)

	jar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar: jar,
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, h.url, nil)
	if err != nil {
		return res, fmt.Errorf("error while creating request: %w", err)
	}

	{
		u, _ := url.Parse(h.url)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		return res, fmt.Errorf("error while getting page: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return res, fmt.Errorf("status code is not 200 but %d", resp.StatusCode)
	}

	// Parse the html
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return res, fmt.Errorf("error while parsing the html: %w", err)
	}

	imgURL := ""
	picBox := doc.Find("section#picBox")
	if picBox.Length() == 0 {
		return res, fmt.Errorf("no picBox found")
	}

	boxBody := picBox.Find("div.boxbody")
	if boxBody.Length() == 0 {
		return res, fmt.Errorf("no boxbody found")
	}
	if boxBody.Children().Length() == 0 {
		return res, fmt.Errorf("no picture after boxbody found")
	}

	img := boxBody.Children().First()
	if val, exists := img.Attr("onclick"); exists {
		start := strings.Index(val, "'") + 1
		end := strings.Index(val, "';")

		if start > -1 && end > start {
			imgURL = val[start:end]
		}
	} else {
		imgURL = img.AttrOr("src", "")
	}

	if imgURL == "" {
		return res, fmt.Errorf("could not find image")
	}

	if strings.HasPrefix(imgURL, "//") {
		imgURL = fmt.Sprintf("%s:%s", func() string {
			p, _ := url.Parse(h.url)
			return p.Scheme
		}(), imgURL)
	}

	imgURL = insertEnterAgreeOnURL(imgURL)

	if o.FileName == "" {
		p, _ := url.Parse(imgURL)
		o.FileName = path.Base(p.Path)
	}

	dest := path.Join(o.Destination, sanitizeNameForPath(o.Artist))
	res.FilePath = path.Join(dest, o.FileName)

	if _, err := createFolderIfNotExists(dest); err != nil {
		return res, fmt.Errorf("cannot create directory: %w", err)
	}

	o.Destination = dest
	return res, downloadImage(client, imgURL, o)
}

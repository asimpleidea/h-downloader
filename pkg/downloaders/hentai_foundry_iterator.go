// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/PuerkitoBio/goquery"
	"github.com/rs/zerolog/log"
)

type hentaiFoundryLinksIterator struct {
	defaultOpts *DownloadOptions
	links       []string
	nextPageURL string
	index       int
}

func NewHentaiFoundryDownloader(webpage string, opts *DownloadOptions) (*hentaiFoundryLinksIterator, error) {
	uri, err := url.Parse(webpage)
	if err != nil {
		return nil, fmt.Errorf("error while parsing url: %w", err)
	}

	downloadMode, err := parseHentaiFoundryURL(uri)
	if err != nil {
		return nil, err
	}

	if opts == nil {
		opts = &DownloadOptions{}
	}

	if opts.Artist == "" {
		opts.Artist = getArtistNameFromHentaiFoundryURL(uri)
	}

	iterator := &hentaiFoundryLinksIterator{
		defaultOpts: opts,
	}

	if downloadMode == singularMode {
		iterator.links = []string{uri.String()}
	} else {
		iterator.nextPageURL = buildHentaiFoundryPageURL(opts.Artist, 1)
	}

	return iterator, nil
}

func (h *hentaiFoundryLinksIterator) Next(ctx context.Context) (Downloader, error) {
	for h.index < len(h.links) {
		dw := &hentaiFoundryPictureDownloader{
			url:            h.links[h.index],
			defaultOptions: h.defaultOpts.Clone(nil),
		}

		h.index++
		return dw, nil
	}

	if h.nextPageURL != "" {
		nextPageURL, links, err := h.getPicturesFromHentaiFoundryArtist(ctx, h.nextPageURL)
		if err != nil {
			h.nextPageURL = ""
			return nil, err
		}

		h.nextPageURL = nextPageURL
		h.links = append(h.links, links...)
		return h.Next(ctx)
	}

	return nil, IteratorDone
}

func (h *hentaiFoundryLinksIterator) getPicturesFromHentaiFoundryArtist(ctx context.Context, webpage string) (string, []string, error) {
	// Prepare the request
	webpage = insertEnterAgreeOnURL(webpage)
	client := &http.Client{}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, webpage, nil)
	if err != nil {
		return "", nil, fmt.Errorf("error while preparing request: %w", err)
	}

	{
		u, _ := url.Parse(webpage)
		if cookie := cookiesManager.Get(u.Host); cookie != "" {
			req.Header.Add(cookieHeaderName, cookie)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return "", []string{}, fmt.Errorf("error while getting webpage: %w", err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return "", []string{}, fmt.Errorf("status code is not 200 but %d", res.StatusCode)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return "", []string{}, err
	}

	// Get next page
	nextPageURL := ""
	{
		if nextPage := doc.Find("ul#yw1 > li.next"); nextPage.Length() > 0 {
			if !nextPage.First().HasClass("hidden") {
				if npu := nextPage.ChildrenFiltered("a"); npu.Length() > 0 {
					if np := npu.First().AttrOr("href", ""); np != "" {
						nextPageURL = (&url.URL{
							Scheme: "https",
							Host:   "www.hentai-foundry.com",
							Path:   np,
						}).String()
					}
				}
			}
		}
	}

	links := []string{}
	{
		thumbs := doc.Find("div.thumb_square")
		if thumbs.Length() == 0 {
			return "", []string{}, fmt.Errorf("no pictures have been found")
		}

		for i := 0; i < len(thumbs.Nodes); i++ {
			node := goquery.NewDocumentFromNode(thumbs.Nodes[i])
			thumbLink := node.ChildrenFiltered("a.thumbLink")
			if thumbLink.Length() == 0 {
				log.Error().Msg("could not found href on image, skipping...")
				continue
			}

			link := thumbLink.First().AttrOr("href", "")
			if link == "" {
				log.Error().Msg("found empty link on thumb link, skipping...")
				continue
			}

			imgURL := url.URL{
				Scheme: "https",
				Host:   "www.hentai-foundry.com",
				Path:   link,
			}

			links = append(links, insertEnterAgreeOnURL(imgURL.String()))
		}
	}

	return nextPageURL, links, nil
}

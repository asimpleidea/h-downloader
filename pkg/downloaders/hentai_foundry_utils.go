// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"fmt"
	"net/url"
	"path"
	"strconv"
	"strings"
)

func parseHentaiFoundryURL(uri *url.URL) (downloadMode, error) {
	uriPath := strings.Trim(uri.Path, "/")

	switch {
	case strings.HasPrefix(uriPath, "user"):
		return fullArtistMode, nil
	case strings.HasPrefix(uriPath, "pictures/user"):
		pathElements := strings.Split(uriPath, "/")
		if len(pathElements) == 3 {
			return fullArtistMode, nil
		}

		return singularMode, nil
	default:
		return "", fmt.Errorf("unrecognized url from hentai-foundry")
	}
}

func buildHentaiFoundryPageURL(artist string, page int) string {
	pageURL := url.URL{
		Scheme: "https",
		Host:   "www.hentai-foundry.com",
		Path:   path.Join("pictures", "user", artist, "page", strconv.Itoa(page)),
	}

	return pageURL.String()
}

func getArtistNameFromHentaiFoundryURL(u *url.URL) string {
	artist := ""
	uriPath := strings.Trim(u.Path, "/")
	pathElements := strings.Split(uriPath, "/")

	switch {
	case strings.HasPrefix(uriPath, "user"):
		if len(pathElements) > 0 {
			artist = pathElements[1]
		}
	case strings.HasPrefix(uriPath, "pictures/user"):
		if len(pathElements) > 2 {
			artist = pathElements[2]
		}
	default:
		return ""
	}

	return artist
}

func insertEnterAgreeOnURL(u string) string {
	const enterAgreeVal string = "enterAgree"

	parsedURL, err := url.Parse(u)
	if err != nil {
		// This does not throw an error because it considers the url to be
		// already validated by someone else
		return u
	}

	queries := parsedURL.Query()
	if val := queries.Get(enterAgreeVal); val == "1" {
		return u
	}

	queries.Set(enterAgreeVal, "1")
	parsedURL.RawQuery = queries.Encode()
	return parsedURL.String()
}

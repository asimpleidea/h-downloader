// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

type downloadMode string

const (
	fullArtistMode downloadMode = "full"
	singularMode   downloadMode = "singular"
)

type DownloadOptions struct {
	Artist      string
	FileName    string
	Destination string
	// TODO: other options
}

func (d *DownloadOptions) Clone(overrides *DownloadOptions) *DownloadOptions {
	return &DownloadOptions{
		Artist: func() string {
			if overrides != nil && overrides.Artist != "" {
				return overrides.Artist
			}

			return d.Artist
		}(),
		FileName: func() string {
			if overrides != nil && overrides.FileName != "" {
				return overrides.FileName
			}

			return d.FileName
		}(),
		Destination: func() string {
			if overrides != nil && overrides.Destination != "" {
				return overrides.Destination
			}

			return d.Destination
		}(),
	}
}

// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/patrickmn/go-cache"
)

var DownloadsCache *cache.Cache

func downloadImage(cl *http.Client, url string, opts *DownloadOptions) error {
	resp, err := cl.Get(url)
	if err != nil {
		return fmt.Errorf("error while download file: %w", err)
	}
	defer resp.Body.Close()

	// Create the file
	dest := path.Join(opts.Destination, opts.FileName)
	out, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("error while creating file: %w", err)
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return fmt.Errorf("error while copying file to destination: %w", err)
	}

	return nil
}

func createFolderIfNotExists(dest string) (bool, error) {
	stat, err := os.Stat(dest)
	if err == nil {
		if !stat.IsDir() {
			return false, fmt.Errorf("destination exists but is a file")
		}

		return false, nil
	}

	if !os.IsNotExist(err) {
		return false, fmt.Errorf("error while checking for directory existence: %w", err)
	}

	if err := os.Mkdir(dest, 0755); err != nil && !os.IsExist(err) {
		return false, fmt.Errorf("error while creating folder: %w", err)
	}

	return true, nil
}

func sanitizeNameForPath(dest string) string {
	// First, make it all lowercase
	d := strings.ToLower(dest)

	replace := []string{
		// Languages
		"[english]", "[spanish]", "[chinese]", "[korean]",

		// Special tags
		"[digital]", "{doujins.com}", "[doujins.com]", "(azur lane)",
		"[artist]", "[pixiv]", "[fanbox]", "various",

		// Special characters
		"/", ":", "*", "\\", ">", "<", "\"", "?", "*", "|",
	}

	// Replace
	for _, rep := range replace {
		d = strings.ReplaceAll(d, rep, "")
	}

	return d
}

// Copyright © 2022 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package downloaders

import (
	"context"
	"time"

	"github.com/rs/zerolog"
)

type WorkerOptions struct {
	Batch   int
	Pause   time.Duration
	Max     int
	Timeout time.Duration
	ResChan chan *DownloadResult
}

type DownloadResult struct {
	FromWorker int
	FilePath   string
	URL        string
	Error      error
}

func Work(wctx context.Context, wid int, wopts *WorkerOptions, operations chan Downloader) error {
	l := zerolog.New(zerolog.NewConsoleWriter()).With().Int("worker#", wid).Logger()
	elements := 0
	lastDownload := time.Now().Add(-10 * time.Minute)

	download := func(op Downloader) *DownloadResult {
		ctx, canc := context.WithTimeout(wctx, wopts.Timeout)
		defer canc()

		res, err := op.Download(ctx, nil)
		res.Error = err

		return res
	}

	ticker := time.NewTicker(time.Minute)
	for {
		select {
		case <-wctx.Done():
			ticker.Stop()
			return nil
		case <-ticker.C:
			if time.Since(lastDownload) > wopts.Pause {
				// Reset, so we could immediately start next time.
				elements = 0
			}
		case op := <-operations:
			if op == nil {
				// TODO: why does it receive nil operations?
				continue
			}
			res := download(op)
			res.FromWorker = wid
			wopts.ResChan <- res
			elements++

			if elements%wopts.Batch == 0 {
				l.Info().Msg("cooling down...")

				sleepCtx, sleepCanc := context.WithCancel(wctx)
				if err := coolDown(sleepCtx, wopts.Pause); err != nil {
					ticker.Stop()
					sleepCanc()
					return nil
				}
				sleepCanc()

				elements = 0
			}
		}
	}
}

func coolDown(ctx context.Context, duration time.Duration) error {
	select {
	case <-ctx.Done():
		return context.Canceled
	case <-time.After(duration):
		return nil
	}
}
